module SimpleForm
  module Helpers
    module Validators
      def has_validators?
        @has_validators ||= attribute_name && object.class.respond_to?(:validators_on)
      end

      private

      def attribute_validators
        object.class.validators_on(attribute_name)
      end

      def reflection_validators
        reflection ? object.class.validators_on(reflection.name) : []
      end

      def valid_validator?(validator)
        valid_conditional_validators?(validator) && action_validator_match?(validator)
      end

      def valid_conditional_validators?(validator)
        valid = true
        if validator.options.include? :if
          valid &&= valid_condition?(validator.options[:if])
        end
        if validator.options.include? :unless
          valid &&= !valid_condition?(validator.options[:unless])
        end
        valid
      end
      
      def valid_condition?(condition)
        if condition.is_a? Proc
          condition.parameters.count == 0 ? condition.yield : condition.yield(object)
        elsif condition.is_a? Symbol
          object.send condition
        end
      end

      def action_validator_match?(validator)
        return true if !validator.options.include?(:on)

        case validator.options[:on]
        when :save
          true
        when :create
          !object.persisted?
        when :update
          object.persisted?
        end
      end

      def find_validator(validator)
        attribute_validators.find { |v| validator === v } if has_validators?
      end
    end
  end
end
